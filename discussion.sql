-- List down all the database list
SHOW DATABASE;

CREATE TABLE artist(
	id INT NOT NULL AUTO_INCREMENT,
	name VARCHAR(50) NOT NULL,
	PRIMARY KEY (id)
);


CREATE TABLE albums(
	id INT NOT NULL AUTO_INCREMENT,
    album_title VARCHAR(50) NOT NULL,
    date_release DATE NOT NULL,
    artist_id INT NOT NULL,
    PRIMARY KEY (id),
    CONSTRAINT fk_albums_artist_id
    	FOREIGN KEY (artist_id) REFERENCES artist (id)
    	ON UPDATE CASCADE
    	ON DELETE RESTRICT
);


CREATE TABLE songs(
	id INT NOT NULL AUTO_INCREMENT,
    song_name VARCHAR(50) NOT NULL,
    length TIME NOT NULL,
    genre VARCHAR(50) NOT NULL,
    album_id INT NOT NULL,
    PRIMARY KEY (id),
    CONSTRAINT fk_songs_album_id
    	FOREIGN KEY (album_id) REFERENCES albums (id)
    	ON UPDATE CASCADE
    	ON DELETE RESTRICT
)

CREATE TABLE playlists(
	id INT NOT NULL AUTO_INCREMENT,
    datetime_created DATE NOT NULL,
    user_id INT NOT NULL,
    PRIMARY KEY (id),
    CONSTRAINT fk_playlists_user_id
    	FOREIGN KEY (user_id) REFERENCES users (id)
    	ON UPDATE CASCADE
    	ON DELETE RESTRICT
);

CREATE TABLE playlists_songs(
	id INT NOT NULL AUTO_INCREMENT,
    playlist_id INT NOT NULL,
    song_id INT NOT NULL,
    PRIMARY KEY (id),
    CONSTRAINT fk_playlist_songs_playlist_id
    	FOREIGN KEY (playlist_id) REFERENCES playlists (id)
    	ON UPDATE CASCADE
    	ON DELETE RESTRICT,
    CONSTRAINT fk_playlists_songs_song_id
    	FOREIGN KEY (song_id) REFERENCES songs (id)
    	ON UPDATE CASCADE
    	ON DELETE RESTRICT
);